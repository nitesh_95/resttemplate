package com.planlytx.controller;

import java.util.Arrays;

import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class Controllers {

	@Autowired
	private RestTemplate restTemplate;

	@GetMapping("/getall")
	public void getemployees() throws ParseException {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<String> httpEntity = new HttpEntity<String>(httpHeaders);
		String body = restTemplate
				.exchange("http://localhost:9000/getalldata", HttpMethod.GET, httpEntity, String.class).getBody();

	}

}
